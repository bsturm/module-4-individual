#!/usr/bin/env python
import re


import sys, os
 
if len(sys.argv) < 2:
	sys.exit("Usage: %s filename" % sys.argv[0])
 
filename = sys.argv[1]
 
if not os.path.exists(filename):
	sys.exit("Error: File '%s' not found" % sys.argv[1])
        
class Player:
    
    def __init__(self, name, at_bats, hits):
        self.name = name
        self.at_bats = at_bats
        self.hits = hits
    
    def getAtBats(self):
        return float(self.at_bats)

    def getHits(self):
        return float(self.hits)
    
    def getName(self):
        return self.name
    
    def getAvg(self):
        return float(self.hits/self.at_bats)
    
    def setStats(self, at_bats, hits): #should set the at bats and hits for a certain player obejct
        self.at_bats = at_bats
        self.hits = hits


player_name = re.compile(r"^([A-Z][a-z]+\s[A-Z]\w+)")
at_bats1 = re.compile(r"(\d)(\st)")
hits1 = re.compile(r"(\d)(\sh)")
List = []

   
def player_in_list(Player6): #when to use self vs. self.name
    #self is a player object
    for player1 in List:
        if Player.getName(player1) == name_match:
            Player.setStats(player1, Player.getAtBats(player1) + atBats_match, Player.getHits(player1) + hits_match) #updates the at bats and hits for a player by calling a method from the player class
            return
            #print "AB: %f" % Player.getAtBats(player1)
    List.append(Player6)


f = open(filename)
for line in f:
    currentline = line.rstrip()
    name_match = player_name.match(currentline)
    if name_match is not None:
        name_match = player_name.match(currentline).group(0)
        #print "player: %s" % name_match
    atBats_match = at_bats1.search(currentline)
    if atBats_match is not None:
        atBats_match = float(at_bats1.search(currentline).group(1))
        #print "AB: %f" % atBats_match
    hits_match = hits1.search(currentline)
    if hits_match is not None:
        hits_match = float(hits1.search(currentline).group(1))
        #print "hits %f" % hits_match
    if name_match is not None:
        player5 = Player(name_match, atBats_match, hits_match)
        player_in_list(player5)
        #print "info: %f" % Player.getAtBats(player5)
    
        #print "Read line: %s" % line.rstrip()

f.close()
newList = sorted(List, key=lambda player2: Player.getAvg(player2), reverse=True)
for player3 in newList:
    print "%s: %.3f" % (Player.getName(player3), Player.getAvg(player3))
    #print ": %f" % battingAvg


